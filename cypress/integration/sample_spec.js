const { url } = require('inspector');

describe('Opening a cat page', () => {
    it('Opens up a category page', () => {
        cy.visit('https://beta-aol.ao-qa.com/laundry');
        cy.get('.open-panel').click();
    });

    it('Takes a snapshot of a category page', () => {
        cy.percySnapshot();
    });
});
